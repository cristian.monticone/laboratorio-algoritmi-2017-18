package priority_queue;

import priority_queue.exception.*;

import java.util.Comparator;
import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Monticone Cristian
 */
public class PriorityQueueTests {

  class IntegerComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer i1, Integer i2) {
      return i1.compareTo(i2);
    }

  }

  @Test(expected = EmptyPriorityQueueException.class)
  public void testEmptyPriorityQueueException() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);
    
    q.insert(42);
    q.extractMinimum();
    q.extractMinimum();
  }

  @Test(expected = EmptyPriorityQueueException.class)
  public void testEmptyPriorityQueueException2() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);
    
    q.insert(42);
    q.extractMinimum();
    q.minimum();
  }

  @Test(expected = MissingElementPriorityQueueException.class)
  public void testMissingElementPriorityQueueException() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);

    q.insert(22);
    q.updatePriority(42, 32);
  }

  @Test(expected = KeyDuplicatePriorityQueueException.class)
  public void testKeyDuplicatePriorityQueueException() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);

    q.insert(22);
    q.insert(12);

    q.updatePriority(22, 12);
  }
  @Test(expected = KeyDuplicatePriorityQueueException.class)
  public void testKeyDuplicatePriorityQueueException2() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);

    q.insert(22);
    q.insert(12);
    q.insert(22);
  }

  @Test
  public void testAnEmptyPriorityQueue() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);
  
    assertTrue(q.size() == 0);

    q.insert(42);
    q.extractMinimum();

    assertTrue(q.size() == 0);
  }

  @Test
  public void testCollectionInsertion() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);
    ArrayList<Integer> a = new ArrayList<>();

    a.add(256);
    a.add(64);
    a.add(128);

    q.insert(a);

    assertTrue(q.extractMinimum() == 64);
    assertTrue(q.extractMinimum() == 128);
    assertTrue(q.extractMinimum() == 256);
  }

  @Test
  public void testUpdatePriority() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);
    
    q.insert(42);
    q.insert(22);
    q.insert(32);

    q.updatePriority(22, 10);

    assertTrue(q.extractMinimum() == 10);
    assertTrue(q.extractMinimum() == 32);
    assertTrue(q.extractMinimum() == 42);
  }

  @Test
  public void testConsecutiveUpdatePriority() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);
    
    q.insert(42);
    q.insert(22);
    q.insert(32);

    q.updatePriority(22, 10);
    q.updatePriority(10, 50);
    q.updatePriority(42, 5);

    assertTrue(q.extractMinimum() == 5);
    assertTrue(q.extractMinimum() == 32);
    assertTrue(q.extractMinimum() == 50);
  }

  @Test
  public void testNormalInsertion2() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);

    q.insert(99);
    q.insert(11);
    q.insert(8);
    q.insert(50);
    q.insert(42);

    assertTrue(q.extractMinimum() == 8);
    assertTrue(q.extractMinimum() == 11);
    assertTrue(q.extractMinimum() == 42);
    assertTrue(q.extractMinimum() == 50);
    assertTrue(q.extractMinimum() == 99);
  }

  @Test
  public void testNormalInsertion() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);

    q.insert(23);
    q.insert(11);
    q.insert(42);
    q.insert(22);
    q.insert(9);

    assertTrue(q.extractMinimum() == 9);
    assertTrue(q.extractMinimum() == 11);
    assertTrue(q.extractMinimum() == 22);
    assertTrue(q.extractMinimum() == 23);
    assertTrue(q.extractMinimum() == 42);
  }

  @Test
  public void testCrescentInsertions() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);

    q.insert(-42);
    q.insert(-10);
    q.insert(0);
    q.insert(4);

    assertTrue(q.extractMinimum() == -42);
    assertTrue(q.extractMinimum() == -10);
    assertTrue(q.extractMinimum() == 0);
    assertTrue(q.extractMinimum() == 4);
  }

  @Test
  public void testDescendingInsertions() {
    IntegerComparator intComparator = new IntegerComparator();
    PriorityQueue<Integer> q = new PriorityQueue<>(intComparator);

    q.insert(4);
    q.insert(0);
    q.insert(-10);
    q.insert(-42);

    assertTrue(q.extractMinimum() == -42);
    assertTrue(q.extractMinimum() == -10);
    assertTrue(q.extractMinimum() == 0);
    assertTrue(q.extractMinimum() == 4);
  }
}

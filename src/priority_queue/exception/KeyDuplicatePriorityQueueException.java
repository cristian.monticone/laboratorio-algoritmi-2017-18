package priority_queue.exception;

/**
 * Key duplicate PriorityQueue exception.
 * 
 * @author Monticone Cristian
 */


public class KeyDuplicatePriorityQueueException extends PriorityQueueException {
  public KeyDuplicatePriorityQueueException(String message) {
    super(message);
  }
}

package priority_queue.exception;

/**
 * Missing element PriorityQueue exception.
 * 
 * @author Monticone Cristian
 */


public class MissingElementPriorityQueueException extends PriorityQueueException {
  public MissingElementPriorityQueueException(String message) {
    super(message);
  }
}

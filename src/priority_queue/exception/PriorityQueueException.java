package priority_queue.exception;

/**
 * A generic PriorityQueue exception.
 * 
 * @author Monticone Cristian
 */

public class PriorityQueueException extends RuntimeException {
  public PriorityQueueException(String message) {
    super(message);
  }
}

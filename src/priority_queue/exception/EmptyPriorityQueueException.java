package priority_queue.exception;

/**
 * Empty PriorityQueue exception.
 * 
 * @author Monticone Cristian
 */


public class EmptyPriorityQueueException extends PriorityQueueException {
  public EmptyPriorityQueueException(String message) {
    super(message);
  }
}

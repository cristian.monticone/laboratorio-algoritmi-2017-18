package priority_queue;

import priority_queue.exception.*;

import java.util.Comparator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * This class implement a generic minimum PriorityQueue structure.
 * 
 * @param <T> the generic type of the PriorityQueue.
 * 
 * @author Monticone Cristian
 */

public class PriorityQueue<T> {
  private ArrayList<T> array;
  private Comparator<? super T> comparator;
  private HashMap<T, Integer> hashMap;

  /**
   * It initialize the PriorityQueue with a comparator.
   * 
   * @param comparator a comparator implementing the relation precendence
   * between T objects.
   */
  public PriorityQueue(Comparator<? super T> comparator) {
    this.comparator = comparator;
    this.array = new ArrayList<T>();
    this.hashMap = new HashMap<T, Integer>();
  }

  /**
   * It initialize the PriorityQueue with a comparator and insert each element
   * in the collection.
   * 
   * @param comparator a comparator implementing the relation precendence
   * between T objects.
   * @param collection a collection of element to be inserted in the PriorityQueue.
   */
  public PriorityQueue(Comparator<? super T> comparator, Collection<? extends T> collection) {
    this(comparator);
    insert(collection);
  }

  /**
   * It inserts a T element in the PriorityQueue.
   * 
   * @param element The element to insert.
   */
  public void insert(T element) {
    add(element);
    raise(size());
  }

  /**
   * It inserts each T element in the collection in the PriorityQueue.
   * 
   * @param collection a collection of element to be inserted in the PriorityQueue.
   */
  public void insert(Collection<? extends T> collection) {
    for (T element : collection) {
      add(element);
    }
    
    buildHeap();
  }

  /** 
   * It extracts the minimum T element in the PriorityQueue.
   * 
   * @exception EmptyPriorityQueueException Throwed when trying to extract from
   * an empty priority queue.
   * @return It returns the minimum T element in the PriorityQueue.
   */
  public T extractMinimum() throws EmptyPriorityQueueException {
    if (size() == 0) 
      throw new EmptyPriorityQueueException("Trying to extract from an empty priority queue.");
    T minimum = get(1);
    swap(1, size());
    remove(size());
    if (size() > 0)
      heapify(1);
    return minimum;
  }

  /**
   * It retrieves, but not remove, the minimum T element in the PriorityQueue.
   * 
   * @exception EmptyPriorityQueueException Throwed when trying to extract from
   * an empty priority queue.
   * @return It returns the minimum T element in the PriorityQueue.
   */
  public T minimum() throws EmptyPriorityQueueException {
    if (size() == 0)
      throw new EmptyPriorityQueueException("Trying to get an element from an empty priority queue.");
    return get(1);
  }

  /**
   * It updates the priority of a T element in the PriorityQueue.
   * 
   * @param element The element to update.
   * @param newPriority The new element that take the place of the old.
   */
  public void updatePriority(T element, T newPriority) throws MissingElementPriorityQueueException {
    if (!hashMap.containsKey(element))
      throw new MissingElementPriorityQueueException("Element not found in the priority queue.");

    int i = hashMap.get(element);
    set(i, newPriority);
    if (comparator.compare(element, newPriority) > 0)
      raise(i);
    else
      heapify(i);
  }

  /**
   * It returns the size of the PriorityQueue.
   * 
   * @return the size of the PriorityQueue.
   */
  public int size() {
    return array.size();
  }

  // It returns the i element in the heap.
  private T get(int i) {
    return array.get(i - 1);
  }

  // It removes the i element in the heap.
  private void remove(int i) {
    hashMap.remove(get(i));
    array.remove(i - 1);
  }

  // It set the i heap position with a T element.
  private void set(int i, T element) {
    if (hashMap.containsKey(element))
      throw new KeyDuplicatePriorityQueueException("Element alredy exist in this priority queue.");

    hashMap.remove(get(i));
    hashMap.put(element, i);
    array.set(i - 1, element);
  }

  // It add a T element to the heap.
  private void add(T element) {
    if (hashMap.containsKey(element))
      throw new KeyDuplicatePriorityQueueException("Element alredy exist in this priority queue.");

    array.add(element);
    hashMap.put(element, size());
  }

  // It returns the index parent.
  private int parent(int i) {
    return i/2;
  }

  // It return the i left node.
  private int left(int i) {
    return (2*i <= size()) ? 2*i : i;
  }

  // It returns the i right node.
  private int right(int i) {
    return (2*i + 1 <= size()) ? 2*i + 1 : i;
  }

  // It swaps two T element in the heap.
  private void swap(int i1, int i2) {
    hashMap.remove(get(i1));
    hashMap.remove(get(i2));
    T aux = get(i1);
    array.set(i1 - 1, get(i2));
    array.set(i2 - 1, aux);
    hashMap.put(get(i1), i1);
    hashMap.put(get(i2), i2);
  }

  // It builds the heap from the object array.
  private void buildHeap() {
    for (int i = size()/2; i >= 1; i--)
      heapify(i);
  }

  // It raises up an element to the correct position.
  public void raise(int i) {
    while (i > 1 && comparator.compare(get(parent(i)), get(i)) > 0) {
      swap(i, parent(i));
      i = parent(i);
    }
  }

  // It reconstructs the heap starting from the element in index i.
  private void heapify(int i) {
    // minimum := index of min{heap[i], heap[left(i)], heap[right(i)]}
    int minimum = (comparator.compare(get(i), get(left(i))) < 0) ? i : left(i);
    minimum = (comparator.compare(get(minimum), get(right(i))) < 0) ? minimum : right(i);
    
    if (minimum != i) {
      swap(i, minimum);
      heapify(minimum);
    }
  }
}

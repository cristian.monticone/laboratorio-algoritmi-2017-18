/* 
 * File:   edit_distance.h
 * Author: Cristian Monticone
 *
 * Created on 26 April 2018, 09.51
 */

#ifndef __EDIT_DISTANCE_H__
#define __EDIT_DISTANCE_H__

#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

// It accept two string s1 and s2 and return their edit distance.
int32_t edit_distance(char* s1, char* s2);

#endif // __EDIT_DISTANCE_H__

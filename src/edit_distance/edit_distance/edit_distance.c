/* 
 * File:   edit_distance.c
 * Author: Cristian Monticone
 *
 * Created on 26 April 2018, 09.54
 */

#include "edit_distance.h"

// Edit distance recurvise functions.
static int32_t d_no_op(char* s1, char* s2);
static int32_t d_canc(char* s1, char* s2);
static int32_t d_ins(char* s1, char* s2);

// It return the substring of s without his first char. 
static char* rest(char* s);

// It accept two string s1 and s2 and return their edit distance.
// It return -1 with errno set to EINVAL when an argument is NULL.
int32_t edit_distance(char* s1, char* s2) {
  int32_t min, aux;

  // Invalid input arguments.
  if (s1 == NULL || s2 == NULL) {
    errno = EINVAL;
    return -1;
  }

  // Base cases.
  if (*s1 == 0) return strlen(s2);
  if (*s2 == 0) return strlen(s1);

  // get min{d_no_op(s1,s2); d_canc(s1,s2); d_ins(s1,s2)}
  min = d_no_op(s1, s2);

  aux = d_canc(s1, s2);
  min = (min > aux) ? aux : min;

  aux = d_ins(s1, s2);
  min = (min > aux) ? aux : min;

  return min;
}

// It return the sub-string of "s" without the first char.
static char* rest(char* s) {
  return (*s == 0) ? s : s + 1;
}

// No operation: equal first character.
static int32_t d_no_op(char* s1, char* s2) {
  return (s1[0] == s2[0]) ? edit_distance(rest(s1), rest(s2)) : INT32_MAX;
}

// Removing character.
static int32_t d_canc(char* s1, char* s2) {
  return 1 + edit_distance(s1, rest(s2));
}

// Inserting character.
static int32_t d_ins(char* s1, char* s2) {
  return 1 + edit_distance(rest(s1), s2);
}

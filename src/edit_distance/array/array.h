/* 
 * File:   array.h
 * Author: Cristian Monticone
 *
 * Created on 30 May 2018, 12.49
 */

#ifndef __ARRAY_H__
#define __ARRAY_H__

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define INITIAL_SET_SIZE 512

// Array of void pointers 'array_t'.
typedef struct {
  void** array;
  size_t used;
  size_t size;
} array_t;

// Array initialization.
void array_init(array_t* set);

// It returns a new empty array.
array_t* new_array();

// It returns the array length.
size_t array_length(array_t* set);

// It returns the element pointer in the i position.
// It returns NULL with errno as EINVAL when index is out of bound.
void* array_get(array_t* set, size_t index);

// It inserts a new element.
void array_insert(array_t* set, void* element);

// It performs an array reset.
void array_reset(array_t* set);

// It only free the array_t structure, not the inside elements!
void array_free(array_t* set);

// It removes an element by his pointer.
void array_remove_element_by_pointer(array_t* set, void* element_p);

// It removes a element by an array index.
void array_remove_element_by_index(array_t* set, size_t index);

#endif // __ARRAY_H__

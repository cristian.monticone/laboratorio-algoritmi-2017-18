/* 
 * File:   array_tests.c
 * Author: Cristian Monticone
 *
 * Created on 30 May 2018, 12.51
 */

#include <stdio.h>
#include <stdlib.h>

#include "unity.h"
#include "array.h"

/*
 * Test suite for dynamic array functions. 
 */

void test_empty_array(void) {
  array_t* a = new_array();

  TEST_ASSERT_EQUAL_INT(0, array_length(a));
}

void test_normal_array(void) {
  array_t* a = new_array();

  char* s1 = "mare";
  char* s2 = "montagna";
  char* s3 = "spiaggia";

  array_insert(a, s1);
  array_insert(a, s2);
  array_insert(a, s3);

  TEST_ASSERT_EQUAL_INT(3, array_length(a));
  TEST_ASSERT_TRUE( array_get(a, 0) == s1 );
  TEST_ASSERT_TRUE( array_get(a, 1) == s2 );
  TEST_ASSERT_TRUE( array_get(a, 2) == s3 );
}

void test_remove_element_by_index(void) {
  array_t* a = new_array();

  char* s1 = "mare";
  char* s2 = "collina";

  array_insert(a, s1);
  array_insert(a, s2);

  TEST_ASSERT_EQUAL_INT(2, array_length(a));

  array_remove_element_by_index(a, 0);

  TEST_ASSERT_EQUAL_INT(1, array_length(a));
  TEST_ASSERT_TRUE( array_get(a, 0) == s2 );
}

void test_remove_element_by_pointer(void) {
  array_t* a = new_array();

  char* s1 = "mare";
  char* s2 = "collina";

  array_insert(a, s1);
  array_insert(a, s2);

  TEST_ASSERT_EQUAL_INT(2, array_length(a));

  array_remove_element_by_pointer(a, s2);

  TEST_ASSERT_EQUAL_INT(1, array_length(a));
  TEST_ASSERT_TRUE( array_get(a, 0) == s1 );
}

void test_out_of_bound_access(void) {
  array_t* a = new_array();

  char* s1 = "montagna";

  array_insert(a, s1);

  char* result = array_get(a, 42);

  TEST_ASSERT_TRUE( errno == EINVAL );
  TEST_ASSERT_TRUE( result == NULL );
}

int main(void) {

  //test session
  UNITY_BEGIN();
  
  RUN_TEST(test_empty_array);
  RUN_TEST(test_normal_array);
  RUN_TEST(test_remove_element_by_index);
  RUN_TEST(test_remove_element_by_pointer);
  RUN_TEST(test_out_of_bound_access);
  
  return UNITY_END();
}

/* 
 * File:   array.c
 * Author: Cristian Monticone
 *
 * Created on 30 May 2018, 12.49
 */

#include "array.h"

static void decrease_length(array_t* set);
static void increase_length(array_t* set);

// It returns a new empty array.
array_t* new_array() {
  array_t* set;

  set = (array_t*)malloc(sizeof(array_t));
  array_init(set);

  return set;
}

// Array initialization.
void array_init(array_t* set) {
  set->array = (void**)malloc(INITIAL_SET_SIZE * sizeof(void*));
  set->used = 0;
  set->size = INITIAL_SET_SIZE;
}

// It inserts a new element.
void array_insert(array_t* set, void* element) {
  set->array[set->used] = element;

  increase_length(set);
}

// It removes an element by his pointer.
void array_remove_element_by_pointer(array_t* set, void* element_p) {
  for (size_t i = 0; i < set->used; i++)
    if (set->array[i] == element_p) {
      array_remove_element_by_index(set, i);
      i = set->used; // Stop after element removed.
    }
}

// It removes a element by an array index.
void array_remove_element_by_index(array_t* set, size_t index) {
  decrease_length(set);
  set->array[index] = set->array[set->used];
}

// It performs an array reset.
void array_reset(array_t* set) {
  free(set->array);
  array_init(set);
}

// It only free the array_t structure, not the inside elements!
void array_free(array_t* set) {
  free(set->array);
  free(set);
}

// It returns the array length.
size_t array_length(array_t* set) {
  return set->used;
}

// It returns the element pointer in the i position.
void* array_get(array_t* set, size_t index) {
  if (index >= set->used) {
    errno = EINVAL;
    return NULL;
  }

  return set->array[index];
}

// It increases the array length by one.
static void increase_length(array_t* set) {
  set->used++;

  if (set->used - 1 == set->size) {
    set->size *= 2;
    set->array = (void**)realloc(set->array, set->size * sizeof(void*));
  }
}

// It decreases the array length by one.
static void decrease_length(array_t* set) {
  set->used--;

  if (set->used == (set->size / 4)) {
    set->size /= 2;
    set->array = (void**)realloc(set->array, set->size * sizeof(void*));
  }
}

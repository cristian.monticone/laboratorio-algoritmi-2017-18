/* 
 * File:   usage.h
 * Author: Cristian Monticone
 *
 * Created on 29 May 2018, 23.56
 */

#ifndef __USAGE_H__
#define __USAGE_H__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include "../array/array.h"
#include "../edit_distance_dyn/edit_distance_dyn.h"

#define LINE_BUFFER_LEN 256

#endif // __USAGE_H__

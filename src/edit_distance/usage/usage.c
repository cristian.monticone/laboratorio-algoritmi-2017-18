/* 
 * File:   usage.c
 * Author: Cristian Monticone
 *
 * Created on 29 May 2018, 23.57
 */

#include "usage.h"

static void file_open(int argc, char** argv);
static void usage(char** argv);
static void load_dictionary(array_t* dictionary, char* path);
static void load_words_to_correct(array_t* words_to_correct, char* path);
static array_t* get_min_words(char* word, array_t* dictionary);
static void correct_and_print(array_t* words_to_correct, array_t* dictionary);

static FILE * file_to_correct;
static FILE * dictionary_file;

int main(int argc, char** argv) {
  file_open(argc, argv);

  array_t* dictionary = new_array();
  array_t* words_to_correct = new_array();

  load_dictionary(dictionary, argv[1]);
  load_words_to_correct(words_to_correct, argv[2]);

  correct_and_print(words_to_correct, dictionary);

  array_free(dictionary);
  array_free(words_to_correct);

  fclose(file_to_correct);
  fclose(dictionary_file);
}

// It shows the properly usage of this program.
static void usage(char** argv) {
  fprintf(stderr, "usage: %s [file_to_correct_path] [dictionary_file_path]\n", argv[0]);
  exit(EXIT_FAILURE);
}

// It checks the arguments and open them as file.
static void file_open(int argc, char** argv) {
  // Arguments number check.
  if (argc < 3) {
    fprintf(stderr, "Too few arguments: %i provided!\n", argc - 1);
    usage(argv);
  }

  // Opening files and handle errors.
  file_to_correct = fopen(argv[1], "r");

  if (file_to_correct == NULL) {
    fprintf(stderr, "Opening the file '%s':\n"
                    "Error #%d: %s\n",
		    argv[1], errno, strerror(errno));
    exit(EXIT_FAILURE);
  }

  dictionary_file = fopen(argv[2], "r");

  if (dictionary_file == NULL) {
    fprintf(stderr, "Opening the file '%s':\n"
                    "Error #%d: %s\n",
		    argv[2], errno, strerror(errno));
    fclose(file_to_correct);
    exit(EXIT_FAILURE);
  }
}

static void load_dictionary(array_t* dictionary, char* path) {
  char line_buffer[LINE_BUFFER_LEN];
  char* element_p;

  while (fgets(line_buffer, LINE_BUFFER_LEN, dictionary_file)) {
    element_p = malloc(strlen(line_buffer)*sizeof(char));
    sscanf(line_buffer, "%s", element_p);
    array_insert(dictionary, element_p);
  }
}

static void load_words_to_correct(array_t* words_to_correct, char* path) {
  char line_buffer[LINE_BUFFER_LEN];
  char* element_p;

  while (fscanf(file_to_correct, "%s", line_buffer) == 1) {
    element_p = malloc((strlen(line_buffer) + 1)*sizeof(char));
    line_buffer[0] = tolower(line_buffer[0]);    // To avoid maiusc.
    sscanf(line_buffer, "%[a-zA-Z]", element_p); // POSIX regexp to avoid punctuation.
    array_insert(words_to_correct, element_p);
  }
}

static void correct_and_print(array_t* words_to_correct, array_t* dictionary) {
  array_t* min_words;
  char* current_word;

  for (size_t i = 0; i < array_length(words_to_correct); i++) {
    // Get min words set.
    current_word = array_get(words_to_correct, i);
    min_words = get_min_words(current_word, dictionary);
    
    // Print phase.
    printf("%s: ", current_word);
    for (size_t k = 0; k < array_length(min_words); k++) {
      printf("%s; ", array_get(min_words, k));
    }
    printf("\n");
    
  }
}

static array_t* get_min_words(char* word, array_t* dictionary) {
    int32_t min = edit_distance(word, array_get(dictionary, 0));

    array_t* min_words = new_array();
    array_insert(min_words, array_get(dictionary, 0));

    for (size_t j = 1; j < array_length(dictionary); j++) {
      int32_t aux = edit_distance(word, array_get(dictionary, j));
      if (aux == min) {
        array_insert(min_words, array_get(dictionary, j));
      }
      else if (aux < min) {
	array_reset(min_words);
        array_insert(min_words, array_get(dictionary, j));
        min = aux;
      }

      // when min = 0 min_words will contain only the input word.
      if (min == 0) return min_words;
    }

    return min_words;
}

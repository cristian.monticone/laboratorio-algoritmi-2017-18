/* 
 * File:   edit_distance_tests.c
 * Author: Cristian Monticone
 *
 * Created on 3 May 2018, 10.51
 */

#include <stdio.h>
#include <stdlib.h>
#include "unity.h"
#include "edit_distance_dyn.h"

/*
 * Test suite for edit distance functions and algorithms
 */

void test_edit_distance_equal_words(void) {
  TEST_ASSERT_EQUAL_INT(0, edit_distance("cocomero", "cocomero"));
}

void test_edit_distance_empty_words(void) {
  TEST_ASSERT_EQUAL_INT(5, edit_distance("zucca", ""));
}

void test_edit_distance_null_strings(void) {
  int32_t result;

  result = edit_distance(NULL, NULL);

  TEST_ASSERT_EQUAL_INT(-1, result);
  TEST_ASSERT_EQUAL_INT(EINVAL, errno);
}

void test_edit_distance_totally_different_words(void) {
  TEST_ASSERT_EQUAL_INT(8, edit_distance("abcd", "efgh"));
}

void test_edit_distance_normal_words(void) {
  TEST_ASSERT_EQUAL_INT(4, edit_distance("tassa", "passato"));
}

void test_edit_distance_single_char_strings(void) {
  TEST_ASSERT_EQUAL_INT(2, edit_distance("z", "a"));
}

int main(void) {

  //test session
  UNITY_BEGIN();
  
  RUN_TEST(test_edit_distance_equal_words);
  RUN_TEST(test_edit_distance_null_strings);
  RUN_TEST(test_edit_distance_empty_words);
  RUN_TEST(test_edit_distance_single_char_strings);
  RUN_TEST(test_edit_distance_normal_words);
  RUN_TEST(test_edit_distance_totally_different_words);
  
  return UNITY_END();
}

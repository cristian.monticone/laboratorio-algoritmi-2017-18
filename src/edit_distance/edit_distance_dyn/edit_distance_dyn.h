/* 
 * File:   edit_distance_dyn.h
 * Author: Cristian Monticone
 *
 * Created on 26 April 2018, 10.49
 */

#ifndef __EDIT_DISTANCE_DYN_H__
#define __EDIT_DISTANCE_DYN_H__

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

#define NEGATIVE_BYTE 255

// It accept two string s1 and s2 and return their edit distance.
int32_t edit_distance(char* s1, char* s2);

#endif // __EDIT_DISTANCE_DYN_H__

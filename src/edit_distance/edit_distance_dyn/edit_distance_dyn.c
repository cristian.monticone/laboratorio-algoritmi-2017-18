/* 
 * File:   edit_distance_dyn.c
 * Author: Cristian Monticone
 *
 * Created on 26 April 2018, 10.48
 */

#include "edit_distance_dyn.h"

// Edit distance recurvise functions.
static int32_t d_no_op(char* s1, char* s2, int32_t i1, int32_t i2);
static int32_t d_canc(char* s1, char* s2, int32_t i1, int32_t i2);
static int32_t d_ins(char* s1, char* s2, int32_t i1, int32_t i2);

// It return the substring of s without his first char. 
static char* rest(char* s);

// Dynamic edit distance helper.
int32_t edit_distance_helper(char* s1, char* s2, int32_t i1, int32_t i2);

// Memoization array.
static int32_t* memo_array;
static int32_t row_size;

// It accept two string s1 and s2 and return their edit distance.
// It return -1 with errno set to EINVAL when an argument is NULL.
int32_t edit_distance(char* s1, char* s2) {
  int32_t result, s1_len, s2_len, memo_size;

  // Invalid input arguments.
  if (s1 == NULL || s2 == NULL) {
    errno = EINVAL;
    return -1;
  }

  s1_len = strlen(s1);
  s2_len = strlen(s2);

  memo_size = s1_len*s2_len;

  // Allocate and set the memo.
  memo_array = malloc(sizeof(int32_t)*memo_size);
  memset(memo_array, NEGATIVE_BYTE, sizeof(int32_t)*memo_size);
  row_size = s1_len;
  
  result = edit_distance_helper(s1, s2, 0, 0);

  free(memo_array);

  return result;
}

// Dynamic edit distance function helper.
int32_t edit_distance_helper(char* s1, char* s2, int32_t i1, int32_t i2) {
  int32_t min, aux;

  // Base cases.
  if (*s1 == 0) return strlen(s2);
  if (*s2 == 0) return strlen(s1);

  // Is this already in the memo??
  if (memo_array[i1 + i2*row_size] >= 0)
    return memo_array[i1 + i2*row_size];

  // get min{d_no_op(s1,s2); d_canc(s1,s2); d_ins(s1,s2)}
  min = d_no_op(s1, s2, i1, i2);

  aux = d_canc(s1, s2, i1, i2);
  min = (min > aux) ? aux : min;

  aux = d_ins(s1, s2, i1, i2);
  min = (min > aux) ? aux : min;

  memo_array[i1 + i2*row_size] = min;

  return min;
}

// It return the sub-string of "s" without the first char.
static char* rest(char* s) {
  return (*s == 0) ? s : s + 1;
}

// No operation: equal first character.
static int32_t d_no_op(char* s1, char* s2, int32_t i1, int32_t i2) {
  return (s1[0] == s2[0]) ? edit_distance_helper(rest(s1), rest(s2), i1 + 1, i2 + 1) : INT32_MAX;
}

// Removing character.
static int32_t d_canc(char* s1, char* s2, int32_t i1, int32_t i2) {
  return 1 + edit_distance_helper(s1, rest(s2), i1, i2 + 1);
}

// Inserting character.
static int32_t d_ins(char* s1, char* s2, int32_t i1, int32_t i2) {
  return 1 + edit_distance_helper(rest(s1), s2, i1 + 1, i2);
}

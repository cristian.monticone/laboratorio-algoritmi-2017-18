package sort;

import java.util.Comparator;
import java.util.ArrayList;

/**
 * This class provide two static methods implementing the InsertionSort 
 * and MergeSort algortihm on generic ArrayList.
 * 
 * @author Monticone Cristian
 */

public final class Sort {

  /**
   * It sort an array using the InsertionSort algorithm.
   * 
   * @param <T> the generic type of the array.
   * @param array the array to be sorted.
   * @param comparator a comparator implementing the relation precendence
   * between T objects.
   */
  public static <T> void insertionSort(ArrayList<T> array, Comparator<? super T> comparator) {
    if (array == null) return;

    for (int i = 1; i < array.size(); i++) {
      // INV: array[1..i] ordered.
      int j = i;
      while (j > 0 && comparator.compare(array.get(j - 1), array.get(j)) > 0) {
        // INV:  ∀ key ∈ array[j+1..i]. array[j] <= key
        Sort.<T>swap(array, j, j-1);
        j--;
      }
    }
  }

  /**
   * It sort an array using the MergeSort algorithm.
   * 
   * @param <T> the generic type of the array.
   * @param array the array to be sorted.
   * @param comparator a comparator implementing the relation precendence
   * between T objects.
   */
  public static <T> void mergeSort(ArrayList<T> array, Comparator<? super T> comparator) {
    if (array == null) return;

    Sort.<T>mergeSortHelper(array, comparator, 0, array.size()-1);
  }

  // MergeSort helper, the algorithm core.
  private static <T> void mergeSortHelper(ArrayList<T> array, 
                                          Comparator<? super T> comparator, 
                                          int left, int right) {
    if (left < right) {
      int center = (left + right) / 2;
      Sort.mergeSortHelper(array, comparator, left, center);
      Sort.mergeSortHelper(array, comparator, center+1, right);
      Sort.merge(array, comparator, left, center, right);
    }
  }

  // It merge two ordered contiguous part in array.
  private static <T> void merge(ArrayList<T> array, 
                                Comparator<? super T> comparator, 
                                int left, int center, int right) {
    ArrayList<T> auxiliar = new ArrayList<>();

    // Clone the array parts to an auxiliar one.
    for (int i = left; i <= right; i++) {
      auxiliar.add(array.get(i));
    }

    // Counters initialization.
    int i = left;
    int j = center + 1;
    int k = left;

    // Merge right and left sub-array to the original one.
    while (i <= center && j <= right) {
      if (comparator.compare(auxiliar.get(i-left), auxiliar.get(j-left)) <= 0) {
        array.set(k, auxiliar.get(i-left));
        i++;
      }
      else {
        array.set(k, auxiliar.get(j-left));
        j++;
      }
      k++;
    }

    // Copy remained left elements.
    while (i <= center) {
      array.set(k, auxiliar.get(i-left));
      i++;
      k++;
    }
  
    // Copy remained right elements.
    while (j <= right) {
      array.set(k, auxiliar.get(j-left));
      j++;
      k++;
    }
  }

  // It swap two T element in the ArrayList.
  private static <T> void swap(ArrayList<T> array, int i1, int i2) {
    T aux = array.get(i1);
    array.set(i1, array.get(i2));
    array.set(i2, aux);
  }
}

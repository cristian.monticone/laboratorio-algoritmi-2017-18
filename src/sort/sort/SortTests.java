package sort;

import java.util.Comparator;
import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Monticone Cristian
 */
public class SortTests {

  class IntegerComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer i1, Integer i2) {
      return i1.compareTo(i2);
    }

  }

  @Test
  public void testInsertionSortNullArray() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = null;

    Sort.insertionSort(array, intComparator);

    assertNull(array);
  }

  @Test
  public void testInsertionSortEmptyArray() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();

    Sort.insertionSort(array, intComparator);

    assertEquals(array.toString(), "[]");
  }

  @Test
  public void testInsertionOneElementArray() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();

    array.add(42);
    
    Sort.insertionSort(array, intComparator);

    assertEquals(array.toString(), "[42]");
  }

  @Test
  public void testInsertionNormalCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(42);
    array.add(12);
    array.add(89);
    array.add(-23);
    array.add(22);

    Sort.insertionSort(array, intComparator);

    assertEquals(array.toString(), "[-23, 12, 22, 42, 89]");
  }

  @Test
  public void testInsertionWorstCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(42);
    array.add(22);
    array.add(10);
    array.add(-23);

    Sort.insertionSort(array, intComparator);

    assertEquals(array.toString(), "[-23, 10, 22, 42]");
  }

  @Test
  public void testInsertionBestCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(-42);
    array.add(22);
    array.add(42);
    array.add(87);
    array.add(143);

    Sort.insertionSort(array, intComparator);

    assertEquals(array.toString(), "[-42, 22, 42, 87, 143]");
  }

  @Test
  public void testInsertionEqualCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(42);
    array.add(42);
    array.add(42);
    array.add(42);
    array.add(42);

    Sort.insertionSort(array, intComparator);

    assertEquals(array.toString(), "[42, 42, 42, 42, 42]");
  }

  @Test
  public void testMergeSortNullArray() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = null;

    Sort.mergeSort(array, intComparator);

    assertNull(array);
  }

  @Test
  public void testMergeSortEmptyArray() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();

    Sort.mergeSort(array, intComparator);

    assertEquals(array.toString(), "[]");
  }

  @Test
  public void testMergeOneElementArray() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();

    array.add(42);
    
    Sort.mergeSort(array, intComparator);

    assertEquals(array.toString(), "[42]");
  }

  @Test
  public void testMergeNormalCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(42);
    array.add(12);
    array.add(89);
    array.add(-23);
    array.add(22);

    Sort.mergeSort(array, intComparator);

    assertEquals(array.toString(), "[-23, 12, 22, 42, 89]");
  }

  @Test
  public void testMergeWorstCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(42);
    array.add(22);
    array.add(10);
    array.add(-23);

    Sort.mergeSort(array, intComparator);

    assertEquals(array.toString(), "[-23, 10, 22, 42]");
  }

  @Test
  public void testMergeBestCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(-42);
    array.add(22);
    array.add(42);
    array.add(87);
    array.add(143);

    Sort.mergeSort(array, intComparator);

    assertEquals(array.toString(), "[-42, 22, 42, 87, 143]");
  }

  @Test
  public void testMergeEqualCase() {
    IntegerComparator intComparator = new IntegerComparator();
    ArrayList<Integer> array = new ArrayList<>();
    
    array.add(42);
    array.add(42);
    array.add(42);
    array.add(42);
    array.add(42);

    Sort.mergeSort(array, intComparator);

    assertEquals(array.toString(), "[42, 42, 42, 42, 42]");
  }

}

package sortusage;

import sort.Sort;

import java.util.Comparator;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This class implement the first usage for the exercise one.
 * 
 * @author Monticone Cristian
 */
public class SortFirstUsage {

  private static final Charset ENCODING = StandardCharsets.UTF_8;

  /**
   * @param args the command line arguments. Expecting one arg specifying the
   * "integers.csv" filepath
   */
  public static void main(String[] args) throws Exception {
    if(args.length < 1)
      throw new Exception("usage: SortFirstUsage [file]");
    
    ArrayList<Long> array = new ArrayList<>();
    LongComparator comparator = new LongComparator();

    SortFirstUsage.loadArray(args[0], array);

    Sort.mergeSort(array, comparator);

    System.out.println(SortFirstUsage.finalCheck(array, comparator) ? "OK, array ordered correctly!" :
                                                                      "Array not correctly ordered...");
  }

  // It load all integers into an array.
  private static void loadArray(String filepath, ArrayList<Long> array) throws IOException{

    Path inputFilePath = Paths.get(filepath);

    try(BufferedReader fileInputReader = Files.newBufferedReader(inputFilePath, ENCODING)){
      String line = null;
      while((line = fileInputReader.readLine()) != null){      
        array.add(Long.parseLong(line));
      }
    } 
  }

  // It return true iff the input array is ordered.
  private static <T>boolean finalCheck(ArrayList<T> array, Comparator<? super T> comparator) {
    boolean flag = true;
    int i = 0;
      
    while (i < array.size() - 1 && flag) {
      flag = (comparator.compare(array.get(i), array.get(i+1)) <= 0);
      i++;
    }

    return flag;
  }

}

package sortusage;

import java.util.Comparator;

/**
 * This class implements a Long comparator.
 * 
 * @author Monticone Cristian
 */

class LongComparator implements Comparator<Long> {
  @Override
  public int compare(Long i1, Long i2) {
    return i1.compareTo(i2);
  }
}

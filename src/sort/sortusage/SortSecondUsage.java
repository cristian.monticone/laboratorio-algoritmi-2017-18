package sortusage;

import sort.Sort;

import java.util.Comparator;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This class implement the second usage for the exercise one.
 * 
 * @author Monticone Cristian
 */
public class SortSecondUsage {

  private static final Charset ENCODING = StandardCharsets.UTF_8;

  /**
   * @param args the command line arguments. Expecting one arg specifying the
   * "integers.csv" integers_filepath sum_filepath
   */
  public static void main(String[] args) throws Exception {
    if(args.length < 1)
      throw new Exception("usage: SortFirstUsage [integers_filepath] [sum_filepath]");
    
    ArrayList<Long> array = new ArrayList<>();
    LongComparator comparator = new LongComparator();

    SortSecondUsage.loadArray(args[0], array);

    SortSecondUsage.testSumFile(args[1], array, comparator);
  }

  // It test each integer in the sums file in array with nSum method.
  private static void testSumFile(String sumFilepath, ArrayList<Long> array, Comparator<Long> comparator) throws IOException {
    ArrayList<Long> sumsArray = new ArrayList<>();

    SortSecondUsage.loadArray(sumFilepath, sumsArray);

    for (Long n : sumsArray)
      System.out.println(n + ": " + SortSecondUsage.nSum(n, array, comparator));
  }

  // It return true iff exist a tuple (x, y) such that x + y = n.
  private static boolean nSum(long n, ArrayList<Long> array, Comparator<Long> comparator) {
    Sort.mergeSort(array, comparator);

    int i = 0;
    int j = array.size() - 1;
    
    while (i < j) {
      if (array.get(i) + array.get(j) == n) {
        return true;
      }
      else if (array.get(i) + array.get(j) > n)
        j--;
      else
        i++;
    }

    return false;
  }

  // It load all integers into an array.
  private static void loadArray(String filepath, ArrayList<Long> array) throws IOException{

    Path inputFilePath = Paths.get(filepath);

    try(BufferedReader fileInputReader = Files.newBufferedReader(inputFilePath, ENCODING)){
      String line = null;
      while((line = fileInputReader.readLine()) != null){      
        array.add(Long.parseLong(line));
      }
    } 
  }
}

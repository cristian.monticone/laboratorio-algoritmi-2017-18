package prim;

import graph.Graph;
import graph.util.Adder;

import java.util.Comparator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Monticone Cristian
 */
public class PrimTests {
  // A integer adder class used.
  class IntegerAdder implements Adder<Integer> {
    public Integer sum(Integer x, Integer y) {
      return x + y;
    }
  }

  // A integer comparator class.
  class IntegerComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer i1, Integer i2) {
      return i1.compareTo(i2);
    }
  }

  @Test
  public void testForestWithOneConnectedParts() {
    Graph<Integer, Integer> g = new Graph<>(false, true);
    IntegerAdder iadd = new IntegerAdder();
    IntegerComparator icomp = new IntegerComparator();

    g.addVertex(1);
    g.addVertex(2);
    g.addVertex(3);
    g.addVertex(4);
    g.addVertex(5);
    g.addVertex(6);
    g.addVertex(7);
    g.addVertex(8);
    g.addVertex(9);

    g.addEdge(1, 2, 4);
    g.addEdge(1, 3, 8);
    g.addEdge(2, 3, 11);
    g.addEdge(2, 5, 8);
    g.addEdge(4, 3, 7);
    g.addEdge(4, 6, 6);
    g.addEdge(4, 5, 2);
    g.addEdge(3, 6, 1);
    g.addEdge(7, 6, 2);
    g.addEdge(7, 5, 4);
    g.addEdge(7, 8, 14);
    g.addEdge(7, 9, 10);
    g.addEdge(5, 8, 7);
    g.addEdge(9, 8, 9);

    assertEquals("93", g.getGraphWeight(iadd).toString());

    Graph<Integer, Integer> mstForest = Prim.mstPrim(g, icomp);

    assertEquals("37", mstForest.getGraphWeight(iadd).toString());
  }

  @Test
  public void testForestWithTwoConnectedParts() {
    Graph<Integer, Integer> g = new Graph<>(false, true);
    IntegerAdder iadd = new IntegerAdder();
    IntegerComparator icomp = new IntegerComparator();

    g.addVertex(1);
    g.addVertex(2);
    g.addVertex(3);
    g.addVertex(4);
    g.addVertex(5);
    g.addVertex(6);
    g.addVertex(7);
    g.addVertex(8);
    g.addVertex(9);

    g.addEdge(1, 2, 4);
    g.addEdge(1, 3, 8);
    g.addEdge(2, 3, 11);
    g.addEdge(2, 5, 8);
    g.addEdge(4, 3, 7);
    g.addEdge(4, 6, 6);
    g.addEdge(4, 5, 2);
    g.addEdge(3, 6, 1);
    g.addEdge(7, 6, 2);
    g.addEdge(7, 5, 4);
    g.addEdge(7, 8, 14);
    g.addEdge(7, 9, 10);
    g.addEdge(5, 8, 7);
    g.addEdge(9, 8, 9);

    g.addVertex(0);
    g.addVertex(10);
    g.addVertex(20);
    g.addVertex(30);
    g.addVertex(40);

    g.addEdge(0, 40, 1);
    g.addEdge(0, 10, 2);
    g.addEdge(10, 40, 6);
    g.addEdge(10, 30, 1);
    g.addEdge(10, 20, 5);
    g.addEdge(20, 30, 3);
    g.addEdge(30, 40, 1);

    assertEquals("112", g.getGraphWeight(iadd).toString());

    Graph<Integer, Integer> mstForest = Prim.mstPrim(g, icomp);

    assertEquals("43", mstForest.getGraphWeight(iadd).toString());
  }
}

package prim;

import graph.Graph;
import priority_queue.PriorityQueue;

import java.util.HashMap;
import java.util.Comparator;

/**
 * This final class provide a prim algorithm for weighted graph.
 *
 * @author Monticone Cristian
 */

public final class Prim {
  /**
   * It performs the prim algorithm on a undirect weighted graph.
   *
   * @param V the graph vertex type.
   * @param W the weight type.
   * @param graph a general graph.
   * @param comparator a W weight comparator.
   * @return a forest with every mst from each connected part of the graph.
   */
  public static <V, W> Graph<V, W> mstPrim(Graph<V, W> graph, Comparator<? super W> comparator) {
    Graph<V, W> forest = new Graph<>(false, true); // Undirect and weighted.

    NodeComparator<V, W> nodeComparator = new NodeComparator<>(comparator);
    PriorityQueue<Node<V, W>> queue = new PriorityQueue<>(nodeComparator);

    HashMap<V, Node<V, W>> nodes = new HashMap<>();
    
    for(V vertex : graph.getVertices()) {
      Node<V, W> node = new Node<>(vertex);
      queue.insert(node);
      nodes.put(vertex, node);
    }

    while (queue.size() > 0) {
      Node<V, W> u = queue.extractMinimum();
      forest.addVertex(u.vertex);

      // If the extracted vertex has a parent, add it to the mst graph.
      if (u.parent != null)
        forest.addEdge(u.parent, u.vertex, u.key);

      // ∀adj ∈ Adj[u] 
      for (V adj : graph.getAdjacents(u.vertex)) {
	W edgeWeight = graph.getEdgeWeight(u.vertex, adj);
	Node<V, W> adjNode = nodes.get(adj);
        
	// adj ∈ queue ∧ adj.key > w(u, adj)
        if (!forest.containsVertex(adj) && 
	    (adjNode.key == null || comparator.compare(adjNode.key, edgeWeight) > 0)) {
	  Node<V, W> newNode = new Node<>(adj, edgeWeight, u.vertex);

	  queue.updatePriority(adjNode, newNode);
	  nodes.put(adj, newNode);
	}
      }
    }

    return forest;
  }
}

// A generic prim vertex node.
class Node<V, W> {
  public V vertex;
  public W key;
  public V parent;

  public Node(V vertex, W key, V parent) {
    this.vertex = vertex;
    this.key = key;
    this.parent = parent;
  }

  public Node(V vertex, W key) {
    this(vertex, key, null);
  }

  public Node(V vertex) {
    this(vertex, null, null);
  }
}

// A prim node comparator.
class NodeComparator<V, W> implements Comparator<Node<V, W>> {
  private Comparator<? super W> comparator;

  public NodeComparator(Comparator<? super W> comparator) {
    this.comparator = comparator;
  }
  
  @Override
  public int compare(Node<V, W> n1, Node<V, W> n2) {
    if (n1.key == null) // null → +∞
      return Integer.MAX_VALUE;
    else if (n2.key == null)
      return Integer.MIN_VALUE;
    else
      return comparator.compare(n1.key, n2.key); // It compare only the keys/weights.
  }
}

package graph;

import graph.exception.*;
import graph.util.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class implement a node of the adjacent lists.
 * 
 * @param <V> the generic vertices type.
 * @param <W> the generic edge weight type.
 * 
 * @author Monticone Cristian
 */

// This class has no modifier: is visible only in the graph package.
class AdjacentList<V, W> {
  public V vertex;
  public LinkedList<Pair<V, W>> list;
  public HashMap<V, Integer> hashMap;

  /**
   * It initializes the list with his vertex.
   *
   * @param vertex the vertex of this list.
   */
  public AdjacentList(V vertex) {
    this.vertex = vertex;
    list = new LinkedList<>();
    hashMap = new HashMap<>();
  }

  /**
   * It inserts a new edge on the list without weight.
   * 
   * @param vertex the target vertex of the new edge on the list.
   */
  public void addEdge(V vertex) {
    this.addEdge(vertex, null);
  }

  /**
   * It inserts a new edge on the list with his weight.
   *
   * @param vertex the target vertex of the new edge on the list.
   * @param weight the weight of this edge.
   */
  public void addEdge(V vertex, W weight) {
    if (hashMap.containsKey(vertex))
      throw new EdgeAlreadyExistGraphException("Trying to add an existing edge.");
    
    Pair<V, W> pair = new Pair<>(vertex, weight);

    hashMap.put(vertex, list.size());
    list.add(pair);
  }

  /**
   * It returns the weight of a edge.
   *
   * @param vertex the edge vertex target.
   * @return the edge vertex target weigth.
   */
  public W getWeightTo(V vertex) {
    if (!hashMap.containsKey(vertex))
      throw new EdgeNotFoundGraphException("Edge not found.");

    int i = hashMap.get(vertex);

    return list.get(i).two;
  }
  
  /**
   * It updates the weight of a edge.
   *
   * @param vertex the edge vertex target to update.
   * @param weight the new weight.
   */
  public void updateEdge(V vertex, W weight) {
    if (!hashMap.containsKey(vertex))
      throw new EdgeNotFoundGraphException("Trying to update an inexisting edge.");

    int i = hashMap.get(vertex);

    Pair<V, W> pair = new Pair<>(vertex, weight);

    list.set(i, pair);
  }

  /**
   * It removes the edge with target in vertex.
   *
   * @param vertex target of the edge to remove.
   */
  public void removeEdgeTo(V vertex) {
    if (!hashMap.containsKey(vertex))
      throw new EdgeNotFoundGraphException("Trying to remove an inexisting edge.");
    
    int i = hashMap.get(vertex);

    hashMap.remove(vertex);

    // Get the last element to perform a swap. 
    // (The tuples are swapped to mantain the hashMap consistency.)
    Pair<V, W> last = list.removeLast();

    // Perform the swap only if there is at least one tuple and isn't the last one.
    if (list.size() != i) {
      list.set(i, last);

      // Update hash map.
      hashMap.remove(last.one);
      hashMap.put(last.one, i);
    }
  }

  /**
   * It returns the sum of each edge weight on this list in a direct graph implementation.
   *
   * @param adder the adder of W elements to use.
   * @return the sum of each edge weight on the list.
   */
  public W listWeight(Adder<W> adder) {
    if (list.size() == 0) {
      return null;
    }
    else {
      W acc = null;

      for (Pair<V, W> tuple : list)
        if (acc != null)
          acc = adder.sum(acc, tuple.two);
	else
	  acc = tuple.two;

      return acc;
    }
  }
}

package graph;

import graph.exception.*;
import graph.util.Adder;

import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Monticone Cristian
 */
public class GraphTests {
  // A integer adder class used in some unit tests.
  class IntegerAdder implements Adder<Integer> {
    public Integer sum(Integer x, Integer y) {
      return x + y;
    }
  }

  @Test
  public void testVertexInsertion() {
    Graph<Integer, Void> g = new Graph<>(false, false); // direct and not weighted

    g.addVertex(12);

    assertEquals("[12]", g.getVertices().toString());
  }

  @Test
  public void testVerticesInsertion() {
    Graph<Integer, Void> g = new Graph<>(true, false); // direct and not weighted

    g.addVertex(12);
    g.addVertex(22);
    g.addVertex(42);

    assertEquals("[12, 22, 42]", g.getVertices().toString());
  }

  @Test
  public void testCollectionInsertion() {
    ArrayList<Integer> a = new ArrayList<>();
    Graph<Integer, Void> g = new Graph<>(true, false); // direct and not weighted
  
    a.add(12);
    a.add(22);
    a.add(42);

    g.addVertex(a);

    assertEquals("[12, 22, 42]", g.getVertices().toString());
  }

  @Test
  public void testRemoveVertexOnDirectGraph() {
    Graph<Integer, Void> g = new Graph<>(true, false); // undirect and not weighted
    
    g.addVertex(42);
    g.addVertex(22);
    g.addVertex(32);

    g.addEdge(42, 32);
    g.addEdge(22, 32);
    g.addEdge(42, 22);

    assertEquals("[32]", g.getAdjacents(22).toString());

    g.removeVertex(32);

    assertEquals("[]", g.getAdjacents(22).toString());
    assertEquals("[22]", g.getAdjacents(42).toString());
  }
  @Test
  public void testRemoveVertexOnUndirectGraph() {
    Graph<Integer, Void> g = new Graph<>(false, false); // undirect and not weighted
    
    g.addVertex(42);
    g.addVertex(22);
    g.addVertex(32);

    g.addEdge(42, 32);
    g.addEdge(22, 32);
    g.addEdge(42, 22);

    assertEquals("[32, 42]", g.getAdjacents(22).toString());

    g.removeVertex(32);

    assertEquals("[42]", g.getAdjacents(22).toString());
    assertEquals("[22]", g.getAdjacents(42).toString());
  }
  
  @Test
  public void testRemoveEdgeOnUndirectGraph() {
    Graph<Integer, Void> g = new Graph<>(false, false); // undirect and not weighted

    g.addVertex(42);
    g.addVertex(22);

    g.addEdge(42, 22);

    assertEquals("[22]", g.getAdjacents(42).toString());
    assertEquals("[42]", g.getAdjacents(22).toString());

    g.removeEdge(22, 42);

    assertEquals("[]", g.getAdjacents(22).toString());
    assertEquals("[]", g.getAdjacents(42).toString());
  }

  @Test
  public void testRemoveEdgeOnDirectGraph() {
    Graph<Integer, Void> g = new Graph<>(true, false); // direct and not weighted
  
    g.addVertex(32);
    g.addVertex(64);

    g.addEdge(32, 64);

    assertEquals("[64]", g.getAdjacents(32).toString());
    assertEquals("[]", g.getAdjacents(64).toString());

    g.removeEdge(32, 64);

    assertEquals("[]", g.getAdjacents(32).toString());
    assertEquals("[]", g.getAdjacents(64).toString());
  }

  @Test
  public void testSize() {
    Graph<Integer, Void> g = new Graph<>(true, false); // direct and not weighted

    g.addVertex(1);
    g.addVertex(2);

    assertEquals(2, g.size());
  }

  @Test
  public void testIsEmpty() {
    Graph<Integer, Void> g = new Graph<>(true, false); // direct and not weighted

    assertTrue(g.isEmpty());

    g.addVertex(42);

    assertFalse(g.isEmpty());
  }

  @Test
  public void testGetGraphWeightOnDirectGraph() {
    Graph<Integer, Integer> g = new Graph<>(true, true); // direct and weighted
    IntegerAdder adder = new IntegerAdder();
    
    g.addVertex(42);
    g.addVertex(22);
    g.addVertex(32);

    g.addEdge(42, 32, 11);
    g.addEdge(22, 32, 22);
    g.addEdge(42, 22, 33);

    assertEquals("66", g.getGraphWeight(adder).toString());
  }

  @Test
  public void testGetGraphWeightOnUndirectGraph() {
    Graph<Integer, Integer> g = new Graph<>(false, true); // undirect and weighted
    IntegerAdder adder = new IntegerAdder();
    
    // First tree of the forest.
    g.addVertex(42);
    g.addVertex(22);
    g.addVertex(32);

    g.addEdge(42, 32, 10);
    g.addEdge(22, 32, 10);
    g.addEdge(42, 22, 10);

    // Second tree of the forest.
    g.addVertex(1);
    g.addVertex(2);
    g.addVertex(3);
    g.addVertex(4);
    g.addVertex(5);

    g.addEdge(1, 2, 1);
    g.addEdge(1, 3, 1);
    g.addEdge(3, 2, 1);
    g.addEdge(4, 3, 1);
    g.addEdge(3, 5, 1);
    g.addEdge(4, 5, 1);

    assertEquals("36", g.getGraphWeight(adder).toString());
  }

  @Test
  public void testUnweightedUndirectEdgeInsertion() {
    Graph<Integer, Void> g = new Graph<>(false, false); // undirect and not weighted

    g.addVertex(12);
    g.addVertex(22);

    g.addEdge(12, 22);

    assertEquals("[22]", g.getAdjacents(12).toString());
    assertEquals("[12]", g.getAdjacents(22).toString());
  }

  @Test
  public void testUnweightedDirectEdgeInsertion() {
    Graph<Integer, Void> g = new Graph<>(true, false); // direct and not weighted

    g.addVertex(12);
    g.addVertex(22);
    g.addVertex(42);

    g.addEdge(12, 22);
    g.addEdge(12, 42);
    g.addEdge(42, 12);

    assertEquals("[22, 42]", g.getAdjacents(12).toString());
    assertEquals("[]", g.getAdjacents(22).toString());
  }

  @Test
  public void testGetEdgeWeight() {
    Graph<Integer, Integer> g = new Graph<>(false, true); // undirect and weighted

    g.addVertex(12);
    g.addVertex(22);
    g.addVertex(42);

    g.addEdge(12, 42, 9000);

    assertEquals("9000", g.getEdgeWeight(12, 42).toString());
  }

  @Test
  public void testUpdateWeightOnDirectGraph() {
    Graph<Integer, Integer> g = new Graph<>(true, true); // direct and weighted

    g.addVertex(12);
    g.addVertex(22);
    g.addVertex(42);

    g.addEdge(12, 42, 9000);

    assertEquals("9000", g.getEdgeWeight(12, 42).toString());

    g.updateEdge(12, 42, 3000);

    assertEquals("3000", g.getEdgeWeight(12, 42).toString());
  }

  @Test
  public void testUpdateWeightOnUndirectGraph() {
    Graph<Integer, Integer> g = new Graph<>(false, true); // undirect and weighted

    g.addVertex(12);
    g.addVertex(22);
    g.addVertex(42);

    g.addEdge(12, 42, 9000);

    assertEquals("9000", g.getEdgeWeight(12, 42).toString());

    g.updateEdge(42, 12, 3000);

    assertEquals("3000", g.getEdgeWeight(12, 42).toString());
  }

  @Test
  public void testWeightedUndirectEdgeInsertion() {
    Graph<Integer, Integer> g = new Graph<>(false, true); // undirect and not weighted

    g.addVertex(12);
    g.addVertex(22);

    g.addEdge(12, 22, 3000);

    assertEquals("[22]", g.getAdjacents(12).toString());
    assertEquals("[12]", g.getAdjacents(22).toString());
    
    assertEquals("3000", g.getEdgeWeight(12, 22).toString());
  }

  @Test
  public void testWeightedDirectEdgeInsertion() {
    Graph<Integer, Integer> g = new Graph<>(true, true); // direct and weighted

    g.addVertex(12);
    g.addVertex(22);
    g.addVertex(42);

    g.addEdge(12, 22, 2000);
    g.addEdge(12, 42, 1000);
    g.addEdge(42, 12, 3000);

    assertEquals("[22, 42]", g.getAdjacents(12).toString());
    assertEquals("[]", g.getAdjacents(22).toString());

    assertEquals("1000", g.getEdgeWeight(12, 42).toString());
    assertEquals("2000", g.getEdgeWeight(12, 22).toString());
    assertEquals("3000", g.getEdgeWeight(42, 12).toString());
  }

  @Test(expected = VertexNotFoundGraphException.class)
  public void testVertexNotFoundGraphException() {
    Graph<Integer, Void> g = new Graph<>(false, false); // undirect and not weighted

    g.addVertex(42);

    g.getAdjacents(22);
  }

  @Test(expected = VertexNotFoundGraphException.class)
  public void testVertexNotFoundGraphException2() {
    Graph<Integer, Void> g = new Graph<>(false, false); // undirect and not weighted

    g.addVertex(42);

    g.removeVertex(22);
  }

  @Test(expected = WeightedGraphException.class) 
  public void testWeightedGraphException() {
    Graph<Integer, Void> g = new Graph<>(true, true); // direct and weighted

    g.addVertex(22);
    g.addVertex(42);

    g.addEdge(22, 42);
  }

  @Test(expected = VertexAlreadyExistGraphException.class)
  public void testVertexAlreadyExistGraphException() {
    Graph<Integer, Void> g = new Graph<>(true, true); // direct and weighted
  
    g.addVertex(42);
    g.addVertex(42);
  }

  @Test(expected = EdgeAlreadyExistGraphException.class)
  public void testEdgeAlreadyExistGraphException() {
    Graph<Integer, Void> g = new Graph<>(true, false); // direct and unweighted

    g.addVertex(42);
    g.addVertex(90);

    g.addEdge(42, 90);
    g.addEdge(42, 90);
  }

  @Test(expected = UnweightedGraphException.class)
  public void testUnweightedGraphException() {
    Graph<Integer, Integer> g = new Graph<>(true, false); // direct and unweighted

    g.addVertex(64);
    g.addVertex(128);

    g.addEdge(64, 128, 404);
  }

  @Test(expected = EdgeNotFoundGraphException.class)
  public void testEdgeNotFoundGraphException() {
    Graph<Integer, Integer> g = new Graph<>(true, true); // direct and weighted

    g.addVertex(30);
    g.addVertex(90);

    g.getEdgeWeight(90, 30);
  }
}

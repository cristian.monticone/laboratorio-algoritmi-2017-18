package graph.exception;

/**
 * A generic Graph exception.
 * 
 * @author Monticone Cristian
 */

public class GraphException extends RuntimeException {
  public GraphException(String message) {
    super(message);
  }
}

package graph.exception;

/**
 * A EdgeAlreadyExist graph exception.
 * 
 * @author Monticone Cristian
 */

public class EdgeAlreadyExistGraphException extends GraphException {
  public EdgeAlreadyExistGraphException(String message) {
    super(message);
  }
}

package graph.exception;

/**
 * A VertexAlreadyExist graph exception.
 * 
 * @author Monticone Cristian
 */

public class VertexAlreadyExistGraphException extends GraphException {
  public VertexAlreadyExistGraphException(String message) {
    super(message);
  }
}

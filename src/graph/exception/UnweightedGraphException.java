package graph.exception;

/**
 * A Unweighted graph exception.
 * 
 * @author Monticone Cristian
 */

public class UnweightedGraphException extends GraphException {
  public UnweightedGraphException(String message) {
    super(message);
  }
}

package graph.exception;

/**
 * A Weighted graph exception.
 * 
 * @author Monticone Cristian
 */

public class WeightedGraphException extends GraphException {
  public WeightedGraphException(String message) {
    super(message);
  }
}

package graph.exception;

/**
 * A Edge not found graph exception.
 * 
 * @author Monticone Cristian
 */

public class EdgeNotFoundGraphException extends GraphException {
  public EdgeNotFoundGraphException(String message) {
    super(message);
  }
}

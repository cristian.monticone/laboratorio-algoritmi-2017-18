package graph.exception;

/**
 * A Vertex not found graph exception.
 * 
 * @author Monticone Cristian
 */

public class VertexNotFoundGraphException extends GraphException {
  public VertexNotFoundGraphException(String message) {
    super(message);
  }
}

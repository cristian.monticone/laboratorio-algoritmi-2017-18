#!/bin/bash

# Moving to the bash script dir.
cd $(dirname "$0")

# Resources path.
JUNIT="../Resources/Java/JUnit/junit-4.12.jar"
HAMCREST="../Resources/Java/JUnit/hamcrest-core-1.3.jar"

# cd to subdir.
cd ..

# Compile test runner.
echo "Compiling tests..."
javac -cp .:$JUNIT:$HAMCREST graph/Graph_TestsRunner.java

# Run the sort tests.
echo "Tests running...\nResult:"
java -cp .:$JUNIT:$HAMCREST graph.Graph_TestsRunner

package graph.util;

/**
 * A util class that provide a tuple of two generic elements.
 *
 * @param A the first element type.
 * @param B the second element type.
 *
 * @author Monticone Cristian
 */

public class Pair<A, B> {
  public A one;
  public B two;

  /**
   * It initializes a empty pair.
   */
  public Pair() {
    this(null, null);
  }

  /**
   * It initializes the pair with two elements.
   *
   * @param one the first element.
   * @param two the second element.
   */
  public Pair(A one, B two) {
    this.one = one;
    this.two = two;
  }
}

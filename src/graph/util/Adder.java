package graph.util;

/**
 * This interface provide the definition af an adder for T elements.
 *
 * @param T the generic type of the adder.
 *
 * @author Monticone Cristian
 */
public interface Adder<T> {

  /**
   * It sums the T elements x and y and return the T result.
   *
   * @param x the first operand.
   * @param y the second operand.
   * @return the result adding x and y.
   */
  public T sum(T x, T y);
}

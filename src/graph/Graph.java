package graph;

import graph.exception.*;
import graph.util.*;

import java.util.Collection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.HashSet;
import java.lang.Number;

/**
 * This class implement a generic graph of V elements,
 * the graph can be direct or undirect and weighted or unweighted.
 * 
 * @param <V> the generic vertices type.
 * @param <W> the generic edge weight type.
 * 
 * @author Monticone Cristian
 */

public class Graph<V, W> {
  private boolean isDirect;
  private boolean isWeighted;

  private ArrayList<AdjacentList<V, W>> adjLists;
  private HashMap<V, Integer> adjMap;

  /**
   * It initializes an empty weighed direct graph.
   *
   */
  public Graph() {
    this(true, true);
  }

  /**
   * It initializes an empty graph.
   *
   * @param isDirect true to set a direct graph, false otherwise.
   * @param isWeighed true to set a weighed graph, false otherwise.
   */
  public Graph(boolean isDirect, boolean isWeighted) {
    this.isDirect = isDirect;
    this.isWeighted = isWeighted;
    this.adjLists = new ArrayList<>();
    this.adjMap = new HashMap<>();
  }

  /**
   * It initializes a weighted direct graph with the vertices in the collection.
   *
   * @param vertices a collection of V elements.
   */
  public Graph(Collection<? extends V> vertices) {
    this(vertices, true, true);
  }

  /**
   * It initializes a graph with the vertices in the collection.
   *
   * @param vertices a collection of V elements.
   * @param isDirect true to set a direct graph, false otherwise.
   * @param isWeighed true to set a weighted graph, false otherwise.
   */
  public Graph(Collection<? extends V> vertices, boolean isDirect, boolean isWeighted) {
    this(isDirect, isWeighted);
    this.addVertex(vertices);
  }

  /**
   * It adds a vertex in the graph.
   *
   * @param vertex The V vertex to be inserted.
   */
  public void addVertex(V vertex) {
    if (adjMap.containsKey(vertex))
      throw new VertexAlreadyExistGraphException("Vertex already exist!");

    // Creating an adjacent list.
    AdjacentList<V, W> adjList = new AdjacentList<>(vertex);

    // Update the hashMap and add the node to the array.
    adjMap.put(vertex, adjLists.size());
    adjLists.add(adjList);
  }

  /**
   * It adds each vertex of a collection in the graph.
   *
   * @param vertices a collection of V vertices.
   */
  public void addVertex(Collection<? extends V> vertices) {
    for (V vertex : vertices)
      this.addVertex(vertex);
  }

  /**
   * It removes a vertex from the graph.
   *
   * @param vertex The V vertex to be removed.
   */
  public void removeVertex(V vertex) {
    if (!adjMap.containsKey(vertex))
      throw new VertexNotFoundGraphException("Trying to remove a inexistent vertex.");
    
    // First remove all the edge going to the vertex removed.
    for (AdjacentList<V, W> adjList : adjLists) {
      try {
        removeEdge(adjList.vertex, vertex);
      }
      catch (EdgeNotFoundGraphException e) {
        // Ok, no edge going to the vertex to remove, nothing to do here.
      }
    }

    // Remove the vertex adjList now.
    int i = adjMap.get(vertex);
    adjMap.remove(vertex);
    AdjacentList<V, W> lastList = adjLists.remove(size() - 1); 

    // Not necessary if the removed element was also the last tuple and with no more vertices.
    if (i != size()) { 
      adjLists.set(i, lastList); // Swap the last adjList to the i-position.

      // Remap in the heashmap the last adjList.
      adjMap.remove(lastList.vertex);
      adjMap.put(lastList.vertex, i);
    }
  }
  
  /**
   * It returns an ArrayList with all the vertices of the graph.
   *
   * @return an ArrayList of V elements.
   */
  public ArrayList<V> getVertices() {
    ArrayList<V> array = new ArrayList<>();

    for(AdjacentList<V, W> adjList : adjLists)
      array.add(adjList.vertex);

    return array;
  }
  
  /**
   * It returns an ArrayList with all the adjacents of a vertex.
   *
   * @param vertex The vertex to check.
   * @return an ArrayList of the adjacents of the vertex.
   */
  public ArrayList<V> getAdjacents(V vertex) {
    if (!adjMap.containsKey(vertex))
      throw new VertexNotFoundGraphException("Impossible to get adjacents of a inexistent vertex.");

    ArrayList<V> array = new ArrayList<>();

    int i = adjMap.get(vertex);

    LinkedList<Pair<V, W>> list = adjLists.get(i).list;

    for (Pair<V, W> pair : list)
      array.add(pair.one);

    return array;
  }

  /**
   * It adds an Edge to the unweighted graph.
   *
   * @param vertex1 the first vertex of the couple.
   * @param vertex2 the second vertex of the couple.
   */
  public void addEdge(V vertex1, V vertex2) {
    if (isWeighted)
      throw new WeightedGraphException("Weight expected!");

    if (!adjMap.containsKey(vertex1))
      throw new VertexNotFoundGraphException("vertex1 not found.");

    if (!adjMap.containsKey(vertex2))
      throw new VertexNotFoundGraphException("vertex2 not found.");

    int i1 = adjMap.get(vertex1);
    AdjacentList<V, W> adjList1 = adjLists.get(i1);

    adjList1.addEdge(vertex2);

    if (!isDirect) {
      int i2 = adjMap.get(vertex2);
      AdjacentList<V, W> adjList2 = adjLists.get(i2);

      adjList2.addEdge(vertex1);
    }
  }

  /**
   * It adds an Edge to the weighted graph.
   *
   * @param vertex1 the first vertex of the couple.
   * @param vertex2 the second vertex of the couple.
   * @param weight the edge weight.
   */
  public void addEdge(V vertex1, V vertex2, W weight) {
    if (!isWeighted)
      throw new UnweightedGraphException("Impossible set a edge weight on a unweighted graph!");

    if (!adjMap.containsKey(vertex1))
      throw new VertexNotFoundGraphException("vertex1 not found.");

    if (!adjMap.containsKey(vertex2))
      throw new VertexNotFoundGraphException("vertex2 not found.");

    int i1 = adjMap.get(vertex1);
    AdjacentList<V, W> adjList1 = adjLists.get(i1);

    adjList1.addEdge(vertex2, weight);

    if (!isDirect) {
      int i2 = adjMap.get(vertex2);
      AdjacentList<V, W> adjList2 = adjLists.get(i2);

      adjList2.addEdge(vertex1, weight);
    }
  }

  /**
   * It updates the edge weight of the weighted graph.
   *
   * @param vertex1 the first vertex of the couple.
   * @param vertex2 the second vertex of the couple.
   * @param weight the new edge weight.
   */
  public void updateEdge(V vertex1, V vertex2, W weight) {
    if (!isWeighted)
      throw new UnweightedGraphException("Impossible update a edge weight on a unweighted graph!");

    if (!adjMap.containsKey(vertex1))
      throw new VertexNotFoundGraphException("vertex1 not found.");

    if (!adjMap.containsKey(vertex2))
      throw new VertexNotFoundGraphException("vertex2 not found.");

    int i1 = adjMap.get(vertex1);
    AdjacentList<V, W> adjList1 = adjLists.get(i1);

    adjList1.updateEdge(vertex2, weight);

    if (!isDirect) {
      int i2 = adjMap.get(vertex2);
      AdjacentList<V, W> adjList2 = adjLists.get(i2);

      adjList2.updateEdge(vertex1, weight);
    }
  }

  /**
   * It removes an edge from the unweighted graph.
   *
   * @param vertex1 the first vertex of the couple.
   * @param vertex2 the second vertex of the couple.
   */
  public void removeEdge(V vertex1, V vertex2) {
    if (!adjMap.containsKey(vertex1))
      throw new VertexNotFoundGraphException("vertex1 not found.");

    if (!adjMap.containsKey(vertex2))
      throw new VertexNotFoundGraphException("vertex2 not found.");

    int i1 = adjMap.get(vertex1);
    AdjacentList<V, W> adjList1 = adjLists.get(i1);

    adjList1.removeEdgeTo(vertex2);

    if (!isDirect) {
      int i2 = adjMap.get(vertex2);
      AdjacentList<V, W> adjList2 = adjLists.get(i2);

      adjList2.removeEdgeTo(vertex1);
    }

  }

  /**
   * It returns the weight of an edge.
   *
   * @param vertex1 the first vertex of the couple.
   * @param vertex2 the second vertex of the couple.
   * @return the weight of the edge.
   */
  public W getEdgeWeight(V vertex1, V vertex2) {
    if (!isWeighted)
      throw new UnweightedGraphException("Impossible use getEdgeWeight on a unweighted graph.");

    if (!adjMap.containsKey(vertex1))
      throw new VertexNotFoundGraphException("vertex1 not found.");

    if (!adjMap.containsKey(vertex2))
      throw new VertexNotFoundGraphException("vertex2 not found.");

    int i = adjMap.get(vertex1);
    AdjacentList<V, W> adjList = adjLists.get(i);

    return adjList.getWeightTo(vertex2);
  }

  /**
   * It returns the weight of the weighted graph.
   *
   * @return the weight of all the graph.
   */
  public W getGraphWeight(Adder<W> adder) {
    if (!isWeighted)
      throw new UnweightedGraphException("Impossible get a graph weight on a unweighted graph!");

    if (size() == 0)
      throw new VertexNotFoundGraphException("Impossible get the weight of an empty graph.");

    W res;

    if (!isDirect)
      res = getUndirectGraphWeight(adder);
    else 
      res = getDirectGraphWeight(adder);

    if (res == null)
      throw new UnweightedGraphException("This graph have no edges, impossible get it weight.");

    return res;
  }

  // Method that sum all weight of a direct graph.
  private W getDirectGraphWeight(Adder<W> adder) {
    W acc = null;

    for (AdjacentList<V, W> adjList : adjLists) {
      W aux = adjList.listWeight(adder);

      if (acc == null)
        acc = aux;
      else if (aux != null)
        acc = adder.sum(acc, aux);
    }

    return acc;
  }

  // Method that sum all wight of a direct graph.
  private W getUndirectGraphWeight(Adder<W> adder) {
    HashSet<V> visited = new HashSet<>();
    HashSet<V> border = new HashSet<>();
    W acc = null;

    // Get the weight of all the tree in the graph and sum them. (it can be a forest)
    for (AdjacentList<V, W> adjList : adjLists) {
      if (!visited.contains(adjList.vertex)) {
        border.add(adjList.vertex);
        W aux = getWeightRecDFS(adjList.vertex, null, border, visited, adder);
	acc = accUpdate(acc, aux, adder);
      }
      visited.add(adjList.vertex);
    }

    return acc;
  }

  // Recursive DFS helper for calculate the weight of a undirect graph. (The connected part from the first vertex)
  private W getWeightRecDFS(V vertex, V predecessor, HashSet<V> border, HashSet<V> visited, Adder<W> adder) {
    W acc = null;

    for (V adjacent : getAdjacents(vertex)) {
      if (!border.contains(adjacent)) {
	// Add this vertex in the border.
	border.add(adjacent);

        // Update the accumuled weight.
	W edgeWeight = getEdgeWeight(vertex, adjacent);
	acc = accUpdate(acc, edgeWeight, adder);
	  
	// Recursive DFS visit.
        W aux = getWeightRecDFS(adjacent, vertex, border, visited, adder);
	acc = accUpdate(acc, aux, adder);

	// Set this vertex as fully visited.
	visited.add(adjacent);
      }
      // Is this a vertex on the border? if yes: add them to the acc.
      else if (predecessor != null && adjacent != predecessor && !visited.contains(adjacent)) {
        W aux = getEdgeWeight(vertex, adjacent);
	acc = accUpdate(acc, aux, adder);
      }
    }

    return acc;
  }

  // It updates a W accumulator.
  private W accUpdate(W acc, W element, Adder<W> adder) {
    if (element != null) {
      if (acc != null) {
        return adder.sum(acc, element);
      }
      else {
        return element;
      }
    }
    else {
      return acc;
    }
  }

  /**
   * It returns true iif the graph size is 0, false otherwise.
   *
   * @return true iif size is 0, false otherwise.
   */
  public boolean isEmpty() {
    return (size() == 0);
  }

  /**
   * It returns the size of the graph, aka the number of vertices.
   *
   * @return the size of the graph, aka the number of vertices.
   */
  public int size() {
    return adjLists.size();
  }

  /**
   * It returns the number of edges.
   *
   * @return the number of edges.
   */
  public int edgesNumber() {
    int acc = 0;

    for (AdjacentList<V, W> adjList : adjLists)
      acc += adjList.list.size();

    if (isDirect)
      return acc;
    else
      return acc/2;
  }

  /**
   * It return true iif vertex is conteined in the graph.
   * 
   * @param vertex the vertex to check.
   */
  public boolean containsVertex(V vertex) {
    return adjMap.containsKey(vertex);
  }
}

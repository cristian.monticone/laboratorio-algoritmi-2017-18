package italian_dist_usage;

import graph.Graph;
import graph.exception.*;
import graph.util.Adder;

import prim.Prim;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashSet;
import java.util.Comparator;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.io.File;

/**
 * this class implement the italian dist graph usage.
 * 
 * @author monticone cristian
 */
public class ItalianDistUsage {

  private static final Charset ENCODING = StandardCharsets.UTF_8;

  /**
   * @param args the command line arguments. Expecting one arg specifying the
   * "italian_dist_graph.csv" filepath
   */
  public static void main(String[] args) throws Exception {
    if(args.length < 1)
      throw new Exception("usage: ItalianDistUsage [file]");
    
    ArrayList<String> records = new ArrayList<>();
    Graph<String, Double> distsGraph = new Graph<>(false, true); // Undirect and weighted.

    ItalianDistUsage.loadRecords(args[0], records);

    ItalianDistUsage.graphSet(distsGraph, records);

    // Perfom the prim algorithm on distsGraph.
    DoubleComparator comparator = new DoubleComparator();
    Graph<String, Double> mst = Prim.mstPrim(distsGraph, comparator);

    // Finally: print MST stats!
    ItalianDistUsage.finalStats(mst);
  }

  // It loads all italian dist records into an array.
  private static void loadRecords(String filepath, ArrayList<String> array) throws IOException {
    File file = new File(filepath);
    Scanner input = new Scanner(file);

    while (input.hasNextLine())
      array.add(input.nextLine());

    input.close();
  }

  // It uses the records array to add the verticens and edges in the graph.
  public static void graphSet(Graph<String, Double> distsGraph, ArrayList<String> records) {
    for (String record : records) {
      String[] field = record.split(",");

      // Check if the vertices already are in the graph, if not: insert them.
      if (!distsGraph.containsVertex(field[0]))
        distsGraph.addVertex(field[0]);
      if (!distsGraph.containsVertex(field[1]))
        distsGraph.addVertex(field[1]);

      Double weight = Double.parseDouble(field[2]);
      try {
        distsGraph.addEdge(field[0], field[1], weight);
      }
      catch (EdgeAlreadyExistGraphException e) {
        // Edge alredy in the graph, check if this weight is less than the previous one.
        Double edgeWeight = distsGraph.getEdgeWeight(field[0], field[1]);
	if (edgeWeight > weight) 
	  distsGraph.updateEdge(field[0], field[1], weight);
      }
    }
  }

  // It prints the final stats.
  public static void finalStats(Graph<String, Double> mst) {
    DoubleAdder adder = new DoubleAdder();

    System.out.println("MST nodes: " + mst.size());
    System.out.println("MST edges: " + mst.edgesNumber());
    System.out.printf("MST weight: %f\n", mst.getGraphWeight(adder));
  }
}

// A double adder class.
class DoubleAdder implements Adder<Double> {
  public Double sum(Double x, Double y) {
    return x + y;
  }
}

// A double comparator class.
class DoubleComparator implements Comparator<Double> {
  @Override
  public int compare(Double d1, Double d2) {
    return d1.compareTo(d2);
  }
}
